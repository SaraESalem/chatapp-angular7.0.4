import { Component, OnInit ,ViewChild} from '@angular/core';
import { AgmCoreModule } from '@agm/core';
// import { MapsAPILoader, AgmMap } from '@agm/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  lat: number = 51.191481;
  lng: number = 10.028420;

  lat1: number = 51.177768
  lng1: number =  10.084264;

  lat2: number = 51.191199;
  lng2: number = 10.054530;
  // geocoder:any;

 
  // @ViewChild(AgmMap) map: AgmMap;

  constructor() { }

  ngOnInit() {
    // var mapProp = {
    //   center: new google.maps.LatLng(18.5793, 73.8143),
    //   zoom: 15,
    //   mapTypeId: google.maps.MapTypeId.ROADMAP
    // };
    // this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
  }

}
