import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, from } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import {ChatMessage} from './ChatMessage'

import {UserInfo} from './UserInfo'

@Injectable({
  providedIn: 'root'
})
export class ChatSeriveService {

  private msgListUrl = 'assets/msg-list.json';
  private apiUrl = '/messages';


  constructor(
    private http: HttpClient) { }

  getAllMockMessages(): Observable<ChatMessage[]> {
    return this.http.get<any>(this.msgListUrl)
    .pipe(map(response => response.array));
  }
  getUserInfo(): Promise<UserInfo> {
    //from local storage
    const userInfo: UserInfo = this.getSession()
    return new Promise(resolve => resolve(userInfo));
  }
  getAllChatMessages() : Observable<ChatMessage[]>{
    return this.http.get<any[]>(`${this.apiUrl}/messages/chatessages`)
    .pipe(map(response => response));
  }

  getMessageById(id: number) {
    return this.http.get(`${this.apiUrl}/messages/` + id);
  }

  addNewMessage(msg: ChatMessage) : Observable<ChatMessage> {
    return this.http.post<ChatMessage>(`${this.apiUrl}/messages/newMessage`, msg).pipe(map(response => response));
  }

  setSession(value){
    sessionStorage.setItem('currentUser', JSON.stringify(value));
  }

  getSession():UserInfo{
    return JSON.parse(sessionStorage.getItem('currentUser'));
  }

    
  }
}
