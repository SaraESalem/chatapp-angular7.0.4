export class UserInfo {
  id: number;
  name?: string;
  avatar?: string;
}