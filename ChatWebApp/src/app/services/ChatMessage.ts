export class ChatMessage {
  
  id: string;
  createdByUserId: number;
  createdByUserName: string;
  createdDate: number;
  modifiedDate: number;
  messageText: string;

}

  
