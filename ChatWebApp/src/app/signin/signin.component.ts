import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { ChatSeriveService} from "../services/chat-serive.service";
import { UserInfo } from "../services/UserInfo"

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  user: UserInfo;
  isValid = false;
  constructor(private ChatSerive: ChatSeriveService,private router: Router) { 
    this.user ={ 
      id:0,
      name:''
    }
  }

  ngOnInit() {
    const _user = this.ChatSerive.getSession()
    if(_user){
      this.router.navigate(['home']);
    }

  }

  addUser(nickname: string,id: number): void {
    console.log("button clicked",id,nickname)
    nickname = nickname.trim();

    if (!nickname  || !this.isExist({name: nickname,id:id}) ) { 
      this.isValid = true;
      return; 
    }else{
      this.isValid = false;
      this.user.name = nickname;
      this.user.id = +id;
      this.ChatSerive.setSession(this.user)
      this.router.navigate(['home']);
    }
    
  }
  isExist(user:any):any{
    var Users = [{name: 'anas',id:1}, {name: 'sara',id:2}];
    var ele = Users.find(e => e.name === user.name && e.id === user.id)
    return ele
  }

}
