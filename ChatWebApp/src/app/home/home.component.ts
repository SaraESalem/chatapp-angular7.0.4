import { Component, OnInit } from '@angular/core';
import { ChatSeriveService} from "../services/chat-serive.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  msg =  {

  }

  constructor(private chatService: ChatSeriveService) { }

  ngOnInit() {
    this.getLastMessage()
    window.addEventListener('storage', function(e) {  
      this.getLastMessage()
     }.bind(this));
  }
  getLastMessage(){
    this.chatService.getAllChatMessages().subscribe(res => {
      // this.msg = res.slice(-1)[0];
      var lastMsg;
      var myUserID = this.chatService.getSession().id;
      for(var i = 0; i < res.length; i = i + 1) {
        if(res[i].createdByUserId != myUserID){
          lastMsg = res[i]
        }
      }
      this.msg = lastMsg;
    });

}
}
