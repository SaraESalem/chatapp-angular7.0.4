import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent }   from './home/home.component';
import {ChatComponent} from './chat/chat.component';
import {MapComponent} from  './map/map.component'
import {SigninComponent} from './signin/signin.component'

const routes: Routes = [
  { path: '', redirectTo: '/signin', pathMatch: 'full' },
  { path: 'signin', component: SigninComponent ,data: { title: 'signin' }},
  { path: 'home', component: HomeComponent , data: { title: 'Home' }},
  { path: 'chat', component: ChatComponent , data: { title: 'Chat' }},
  { path: 'map', component: MapComponent , data: { title: 'Map' }}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
