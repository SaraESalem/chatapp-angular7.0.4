import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ChatComponent } from './chat/chat.component';
import { MapComponent } from './map/map.component';
import { HttpClientModule }    from '@angular/common/http';

import { FormsModule } from '@angular/forms'; 
import { AgmCoreModule } from '@agm/core';
import { SigninComponent } from './signin/signin.component';

import { fakeBackendProvider } from './fake-backend/fake-backend';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ChatComponent,
    MapComponent,
    SigninComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDhiKdIIj4a_4mwqdohtwWb2UWGEhYOWbs'
    })
  ],
  providers: [fakeBackendProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
