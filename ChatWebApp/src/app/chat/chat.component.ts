import { Component, OnInit } from '@angular/core';
import { ChatSeriveService} from "../services/chat-serive.service";
import {ChatMessage } from "../services/ChatMessage"
import { UserInfo } from "../services/UserInfo"


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  msgList: ChatMessage[] ;
  user: UserInfo;
  toUser: UserInfo;
  myMsg:ChatMessage; 
  
  constructor(private chatService: ChatSeriveService) { 
    this.myMsg = {
      id:'',
      messageText:'',
      createdByUserId:0,
      createdByUserName:'',
      createdDate:0,
      modifiedDate:0 
    }
      // Get mock user information
      this.chatService.getUserInfo().then((res) => {
        this.user = res
      });    

  }

  ngOnInit() {
    //load mock data from json file and set it in local storage//
    
    this.getAllChatMessages();
    window.addEventListener('storage', function(e) {  
      console.log('Woohoo, someone changed my localstorage via another tab/window!');
      this.getAllChatMessages()
     }.bind(this));
    
  }


  getAllChatMessages(){
    return this.chatService.getAllChatMessages().subscribe(res => {
      this.msgList = res;
      // localStorage.setItem('chatessages', JSON.stringify(res));
    });
  }
 

  sendMsg(_msg){
    console.log(_msg)
    if(_msg != ""){
      this.myMsg.messageText = _msg;
      this.myMsg.createdByUserId = this.chatService.getSession().id;
      this.myMsg.createdByUserName = this.chatService.getSession().name;
      this.myMsg.createdDate = new Date().getTime()
      this.myMsg.modifiedDate = new Date().getTime()
      console.log("arr22 ",this.msgList)
      this.chatService.addNewMessage(this.myMsg)
      .subscribe(data => {
        console.log("data ",data)
        //get data from localstorage and refresh list
        this.getAllChatMessages()
              },
              error => {
                console.log(error)  
               });
    }
   
  }
  


}
