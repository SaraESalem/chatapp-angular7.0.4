import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
 
@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
 
    constructor() { }
 
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // array in local storage for registered  #chatessages#
        let chatessages: any[] = JSON.parse(localStorage.getItem('chatessages')) || [];
 
        // wrap in delayed observable to simulate server api call
        return of(null).pipe(mergeMap(() => {
 
            // entered new message
            if (request.url.endsWith('/messages/newMessage') && request.method === 'POST') {
                // get new message object from post body
                let newMessage = request.body;

                // save new message
                /* 
                     #1.id - number
                2.messageText - string 
                    # 3.createdDate – DateTime 
                    # 4.modifiedDate – DateTime
                5.createdByUserId – number 
                5.createdByUserName – string
                */

                newMessage.id = chatessages.length + 1;
                newMessage.createdDate = new Date().getTime()
                newMessage.modifiedDate = new Date().getTime()
                chatessages.push(newMessage);
                localStorage.setItem('chatessages', JSON.stringify(chatessages));

                // respond 200 OK
                return of(new HttpResponse({ status: 200 ,body:newMessage}));
            }
 
            // get chatessages
            if (request.url.endsWith('/messages/chatessages') && request.method === 'GET') {
                if (chatessages.length > 0) {
                    return of(new HttpResponse({ status: 200, body: chatessages }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ error: { message: 'No Messages' } });
                }
            }
 
            // get message by id
            if (request.url.match(/\/messages\/\d+$/) && request.method === 'GET') {
                    // find messages by id in chatessages array
                    let urlParts = request.url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    let matched = chatessages.filter(msg => { return msg.id === id; });
                    let msg = matched.length ? matched[0] : null;
 
                    return of(new HttpResponse({ status: 200, body: msg }));
             
            }
 
            // pass through any requests not handled above
            return next.handle(request);
             
        }))
 
        .pipe(materialize())
        .pipe(delay(500))
        .pipe(dematerialize());
    }
}
 
export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};