import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { ChatSeriveService} from "./services/chat-serive.service";
import {ChatMessage } from "./services/ChatMessage"


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title: string = "ChatApp";
  msgList: ChatMessage[] ;

  constructor(private router: Router, private activatedRoute: ActivatedRoute,private chatService: ChatSeriveService) {
    console.log("big dad")
    this.getAllMockMessages();
  }
  ngOnInit() {
    this.router.events.pipe(
        filter(event => event instanceof NavigationEnd)
    ).forEach(e => {
      this.title = this.activatedRoute.root.firstChild.snapshot.data['title'];
  });
  const _user = this.chatService.getSession()
  if(!_user){
    this.router.navigate(['signin']);
  }
  
}
getAllMockMessages(){
  return this.chatService.getAllMockMessages().subscribe(res => {
    this.msgList = res;
    localStorage.setItem('chatessages', JSON.stringify(res));
  });
}

}
